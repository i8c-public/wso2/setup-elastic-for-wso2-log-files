# -*- mode: ruby -*-
# vi: set ft=ruby :

# Add required plugins to the required_plugins array of strings variable below:
# 1) vagrant-vbguest
# The centos/7 box doesn't contain the VirtualBox Guest Additions.
# Use the vagrant-vbguest plugin to install them automatically.
# This functionality is required to automatically sync the /vagrant folder between host and guest.
#
# 2) vagrant-reload
# Automatically reload the box using the vagrant-reload plugin
# to enable automatic syncing of /vagrant folder and other stuff after provisioning.
required_plugins = %w( vagrant-vbguest vagrant-reload )
restart_manually = false

required_plugins.each do |plugin|
  if not Vagrant.has_plugin?(plugin)
    system "vagrant plugin install #{plugin}"
    restart_manually = true
  end
end

if restart_manually
  puts "Execute the vagrant utility again now that required plugins have been installed"
  exit
end


# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "centos/7"
  
  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false
  # do NOT download the iso file from a webserver
  
  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # Override the rsync type in the Vagrantfile of the centos/7 box
  config.vm.synced_folder ".", "/vagrant", type: "virtualbox"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  config.vm.provider "virtualbox" do |vb|
    vb.gui = true
    # Adapt the memory and CPU settings below to the specs of your laptop
    vb.memory = "8192"
    vb.cpus = "4"
  # Assign a unique name to your VM
    vb.name = "Elastic-WSO2-VM"
    # Enable copy/paste from host to guest
    vb.customize ["modifyvm", :id, "--clipboard", "bidirectional"]
    vb.customize ["modifyvm", :id, "--draganddrop", "bidirectional"]
  end

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
  
  # Change timezone to Brussels
  config.vm.provision "timezone", privileged: true, type: "shell", inline: "timedatectl set-timezone Europe/Brussels"
  
  # Install GNOME desktop, also installs a lot of other utilities such as wget, firefox, packagekit, JDK 8,...
  config.vm.provision "desktop", privileged: true, type: "shell", inline: "echo 'installing desktop ...' && yum -y groupinstall 'GNOME Desktop' 'Graphical Administration Tools' && systemctl set-default graphical.target && systemctl start graphical.target && echo 'desktop installed!'"
  
  # Disable automatic updates, this may conflict with later yum installation commands
  config.vm.provision "autoupdate-off", privileged: true, type: "shell", inline: "echo 'disabling packagekit ...' && systemctl stop packagekit.service && systemctl mask packagekit.service && echo 'packagekit disabled!'"
  
  # Change keyboard to Belgian layout, remove/update if you use a different keyboard layout
  # The keyboard layout can also be selected upon first login in GNOME
  config.vm.provision "keyboard", privileged: true, type: "shell", inline: "localectl set-x11-keymap be"

    # Install Docker
  config.vm.provision "docker"

  # Install Docker Compose; don't use the vagrant-docker-compose plugin, it installs an outdated docker-compose version
  config.vm.provision "docker-compose", privileged: false, type: "shell", inline: "echo 'installing Docker Compose...' && sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose && sudo chmod +x /usr/local/bin/docker-compose && echo 'Docker Compose installed!'"


  
  # Reload to enable the VirtualBox Guest Additions and keyboard change
  # Use vagrant reload, "shutdown -r" from the guest command line is not sufficient
  config.vm.provision :reload

  config.vm.provision "Wso2Container", type: "docker" do |docker|
    docker.run "Wso2",
    image: "wso2/wso2ei-integrator:6.4.0",
    args: "-it -u 0 -v /wso2logs:/home/wso2carbon/wso2ei-6.4.0/repository/logs -v /vagrant/WSO2-custom-code/ElasticCompositeApplication_1.0.0.car:/home/wso2carbon/wso2ei-6.4.0/repository/deployment/server/carbonapps/ElasticCompositeApplication_1.0.0.car -v /vagrant/WSO2-custom-code/LogMediator-1.0.0.jar:/home/wso2carbon/wso2ei-6.4.0/dropins/LogMediator-1.0.0.jar -v /vagrant/WSO2-custom-code/log4j.properties:/home/wso2carbon/wso2ei-6.4.0/conf/log4j.properties -p 8280:8280 -p 8243:8243 -p 9443:9443"
  end
end
