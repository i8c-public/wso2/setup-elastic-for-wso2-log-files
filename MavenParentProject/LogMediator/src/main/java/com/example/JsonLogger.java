package com.example;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.synapse.MessageContext;
import org.apache.synapse.mediators.AbstractMediator;
import org.json.simple.JSONObject;

public class JsonLogger extends AbstractMediator { 


	
	private static Log log = LogFactory.getLog("MyCustomWSO2Logs");
	
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean mediate(MessageContext arg0) {
		JSONObject obj = new JSONObject();
		obj.put("startStop",arg0.getProperty("startStop"));
		obj.put("messageStartTimeStamp",arg0.getProperty("messageStartTimeStamp"));
		obj.put("sequenceStartTime",arg0.getProperty("sequenceStartTime"));
		obj.put("sequenceEndTime",arg0.getProperty("sequenceEndTime"));
		obj.put("messageId",arg0.getProperty("messageId"));
		obj.put("sendingParty",arg0.getProperty("sendingParty"));
		obj.put("customer",arg0.getProperty("customer"));
		obj.put("serviceSelected",arg0.getProperty("serviceSelected"));
		obj.put("receivingParty",arg0.getProperty("receivingParty"));
		obj.put("sequence",arg0.getProperty("sequence"));
		obj.put("status",arg0.getProperty("status"));
		obj.put("statusMessage",arg0.getProperty("statusMessage"));
		obj.put("synchronous", arg0.getProperty("synchronous"));
		    
		if(arg0.getProperty("errorSequence") != null){
			obj.put("errorSequence", arg0.getProperty("errorSequence"));
		  
			obj.put("original_payload",arg0.getProperty("original_payload_changed"));
		}
		log.info("\n");
		log.info(obj.toJSONString());
		return true;
	}
}
