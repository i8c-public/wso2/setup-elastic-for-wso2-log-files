#!/bin/bash

set -euo pipefail

beat=$1

echo "chown filebeat yml file..."

chmod u=rw,go=r,o=r /usr/share/filebeat/filebeat.yml
#chown root:${beat} /usr/share/${beat}/${beat}.yml
chown root:1000 /usr/share/filebeat/filebeat.yml
#chown root:root /usr/share/${beat}/${beat}.yml
ls -la /usr/share/filebeat/filebeat.yml
#chown root:root /usr/share/${beat}/${beat}.yml
